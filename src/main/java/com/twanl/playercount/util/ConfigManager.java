package com.twanl.playercount.util;

import com.twanl.playercount.PlayerCount;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {

    private PlayerCount plugin = PlayerCount.getPlugin(PlayerCount.class);

    //Files & Config Files
    private FileConfiguration playersC;
    private File playersF;
    //--------------------

    public void setup() {
        playersF = new File(plugin.getDataFolder(), "PlayerData.yml");
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }
        if (!playersF.exists()) {
            try {
                playersF.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        playersC = YamlConfiguration.loadConfiguration(playersF);
    }


    public FileConfiguration getPlayers() {
        return playersC;
    }

    public void savePlayers() {
        playersF = new File(plugin.getDataFolder(), "PlayerData.yml");
        try {
            playersC.save(playersF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reloadplayers() {
        playersC = YamlConfiguration.loadConfiguration(playersF);
    }
}
